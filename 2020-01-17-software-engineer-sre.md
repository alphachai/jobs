### Mid-Senior Software Engineer (SRE)

Why join Mintel? We are the world's leading market intelligence agency and technology underpins our success. We help our clients understand consumers and consumer markets, and we aspire to provide the market intelligence behind every great business decision. Our technology supports research, analysis, and operations work of Mintel staff and help us deliver data, insight and opinion in a compelling way to our clients across the globe.

The Mintel Data Platform is tasked with ingesting, enriching, and publishing marketing data. Our cross-functional team of architects, engineers, and cloud gurus take a holistic, forward-thinking approach to building ETL pipelines, machine learning pipelines, and cloud infrastructure.

Inside the MDP, our team of software engineers acts as the bridge between development and operations by practicing [SRE](https://landing.google.com/sre/sre-book/toc/index.html). Our team is depended on to mentor developers and guide teams towards best practices. To quote the handbook,

> "Unpacking the term a little, first and foremost, SREs are *engineers*. We apply the principles of computer science and engineering to the design and development of computing systems: generally, large distributed ones. Sometimes, our task is writing the software for those systems alongside our product development counterparts; sometimes, our task is building all the additional pieces those systems need, like backups or load balancing, ideally so they can be reused across systems; and sometimes, our task is figuring out how to apply existing solutions to new problems."

**What you will be doing**:

* Working with your team to experiment and make impactful architectural decisions
* Building tools, libraries, and frameworks which make developer's lives easier
* Ensuring optimum performance, high availability, and stability of our container orchestration platform (Docker/ECS)
* Monitoring the container orchestration platform for availability, latency, performance, and system health
* Laying the groundwork for migrating our containers to Kubernetes

**What we're looking for**:

* A software engineer with a passion for automation and an interest in SRE
* 4+ years of professional experience with Python (preferred) or another popular, modern language such as Golang
* Meaningful experience developing with and deploying to Linux
* Knowledge of Docker with professional experience utilizing it for deployments in a micro-service architecture

**Bonus points**:

* Practical experience with Amazon Web Services (AWS) or Google Cloud Platform (GCP)
* Experience deploying infrastructure as code with Terraform
* Experience with designing and implementing CI/CD solutions using Jenkins pipelines in Groovy, Python, Git, Shell, YAML, and Docker
* Experience with container and orchestration technologies such as ECS and Kubernetes

**We hope you'll like our...**

* Openness to new technologies - you and your immediate team have a high level of autonomy when choosing solutions and advancing best-practices
* Flexible working environment with the opportunity to work from home
* Generous PTO policy and flexible working hours
* Personal development time
* Casual dress, free coffee and snacks, regular catered lunches and treats, and the beloved ping-pong table

